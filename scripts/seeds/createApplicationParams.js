#!/usr/bin/env node

const logger = require('@pubsweet/logger')
const db = require('@pubsweet/db-manager/src/db')

const {
  model: ApplicationParameter,
} = require('editoria-data-model/src/applicationParameter')

const config = require('../../config/modules/book-builder')

const truncate = async () => {
  await db.raw(`truncate table application_parameter cascade`)
  logger.info(`truncate table application parameter`)
}

const createApplicationParams = async () => {
  const areas = Object.keys(config)
  await truncate()
  await Promise.all(
    areas.map(async area => {
      const parameters = await new ApplicationParameter({
        context: 'bookBuilder',
        area,
        config: JSON.stringify(config[area]),
      }).save()
      logger.info(
        `New Application Parameter created: ${JSON.stringify(config[area])}`,
      )
      return parameters
    }),
  )
}

module.exports = createApplicationParams
