# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.12](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.11...v2.0.12) (2020-03-03)

### Bug Fixes

- **app:** remove from dep jobxsweet ([3e739a7](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/3e739a740c392262e1da1e14a8ad5bd7a5b3d746))

### [2.0.11](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.10...v2.0.11) (2020-03-03)

<a name="2.0.10"></a>

## [2.0.10](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.9...v2.0.10) (2020-01-31)

<a name="2.0.9"></a>

## [2.0.9](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.8...v2.0.9) (2020-01-09)

### Bug Fixes

- **app:** multiple upload ([fcc3198](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/fcc3198))
- **app:** template files remove fix ([e7b0ecc](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/e7b0ecc))

<a name="2.0.8"></a>

## [2.0.8](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.7...v2.0.8) (2019-11-06)

### Bug Fixes

- **app:** exporter and wax fixes ([3515104](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/3515104))

<a name="2.0.7"></a>

## [2.0.7](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.6...v2.0.7) (2019-10-30)

### Bug Fixes

- **app:** new wax ([ae0fe6b](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/ae0fe6b))

<a name="2.0.6"></a>

## [2.0.6](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.5...v2.0.6) (2019-10-29)

<a name="2.0.5"></a>

## [2.0.5](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.4...v2.0.5) (2019-10-25)

### Bug Fixes

- **app:** dashboard refetching ([e8fcfad](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/e8fcfad))

<a name="2.0.4"></a>

## [2.0.4](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.3...v2.0.4) (2019-10-25)

### Bug Fixes

- **app:** wax escape character ([93234c4](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/93234c4))

<a name="2.0.3"></a>

## [2.0.3](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.2...v2.0.3) (2019-10-25)

### Bug Fixes

- **app:** smaller payload ([fa7fd3f](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/fa7fd3f))

<a name="2.0.2"></a>

## [2.0.2](https://gitlab.coko.foundation/editoria/editoria-vanilla/compare/v2.0.1...v2.0.2) (2019-10-24)

### Bug Fixes

- **app:** mounted volumes ([b87657e](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/b87657e))

<a name="2.0.1"></a>

## 2.0.1 (2019-10-23)

### Bug Fixes

- **app:** jobs added, dynamic port from env ([4183ee4](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/4183ee4))

<a name="2.0.0"></a>

# 2.0.0 (2019-10-18)

### Bug Fixes

- **app:** chapter end notes fix ([a6fee58](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/a6fee58))
- **app:** docker fix and app params seed fix ([a573e08](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/a573e08))
- **app:** new wax version, numbering parts fix book settings modal ([7bd3886](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/7bd3886))
- **app:** update versions ([2d3a9c1](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/2d3a9c1))
- **app:** various fixes and updated dependencies ([9bd2498](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/9bd2498))
- **applicationparameter:** add truncate befores run script ([d2e0009](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/d2e0009))

### Features

- **app:** export overhaul ([2878aa3](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/2878aa3))
- **app:** running headers ([3f7ba77](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/3f7ba77))
- **footernotes:** create endnotes type ([eba670c](https://gitlab.coko.foundation/editoria/editoria-vanilla/commit/eba670c))

<a name="1.1.1"></a>

## [1.1.1](https://gitlab.coko.foundation/editoria/ucp/compare/v1.1.0...v1.1.1) (2018-12-19)

<a name="1.1.0"></a>

# 1.1.0 (2018-11-20)

### Features

- conventional commits added ([b98b958](https://gitlab.coko.foundation/editoria/ucp/commit/b98b958))

# Editoria

## 1.3.0

#### Wax

- Search / find and replace
- Notes pane is hidden if there are no notes in the document
- Note callouts work with track changes
- Track changes previous / next navigation
- Copy / cut and paste works with track changes
- Indicator for how many items exist in a collapsed comment discussion
- Performance improvements
- Find and Replace a single match and undo throws an Error
- Undo / Redo Notes on certain Occasions throws an Error
- Opening an Empty Chapter Save is Enabled and modal is triggered if you try to go back without any change in the Editor
- if Track Changes is on, user cannot remove an image
- Remove additions on cut operation in track changes if done from the same user
- With track Changes on if you accept a deleted note and undo throws an error
- With track Changes on if you delete a whole paragraph and undo throws an error
- Navigate Through Notes with left and right arrow
- Toggle on /off Native Spell Checker
- Full screen mode
- Track Spaces
- Image Captions
- Change note icon in the toolbar
- Add keyboard shortcuts for accept & reject track changes
- Small Caps

#### Maintenance

- Switch to using yarn instead of npm by default
- React 16
- Upgrade to latest pubsweet client
- Clean up component prop types and refs
- Switch to Postgres
- Docker container for the DB provided via yarn start:services

## 1.2.0

- Upgrade to latest Pubsweet server, client, and components
- Introduce password reset

#### Book Builder

- Preview exported book in Vivliostyle Viewer
- Chapters keep track of their own numbering (independently from parts) in the Body
- Renaming of fragments has been removed in favour of using the title tag in Wax
- Drag and drop fixes

#### Wax Editor

- Can now style multiple blocks/paragraphs at once, including lists
- Keyboard shortcuts
  - Turn track changes on/off => ctrl/cmd+y
  - Hide / show tracked changes => ctrl/cmd+shift+y
  - Comments => ctrl/cmd+m
  - Save => ctrl/cmd+s
- New Title style (which will also rename the fragment in the book builder)
- New Subtitle style
- New Bibliography Entry style
- Revamped track changes UI: Tools are now in the toolbar instead of inline, and line height in the document has been reduced
- Surface doesn't lose focus any more unless the user clicks outside of Wax
- Paragraph indentation

## 1.1.4

#### Wax Editor

- Fix for unsaved changes warning

## 1.1.3

- New design for the book builder, the dashboard and the theme

#### Dashboard

- Renamed 'remove' button to 'delete' for consistency with the book builder
- Double clicking on a book will take you to the book builder for that book, instead of opening the renaming interface
- The position of 'Edit' and 'Rename' actions have been swapped ('Edit' now comes first)
- Books now appear in alphabetical order

#### Book Builder

- Fixed issue with fragments disappearing when uploading multiple files
- Renamed 'Front Matter' and 'Back Matter' to 'Frontmatter' and 'Backmatter'

#### Wax Editor

- Introduce layouts
- Accept configuration options (`layout` and `lockWhenEditing`)
- It is now broken down into separate modules for better separation of concerns
  - Pubsweet integration
  - React integration
  - Core
- Diacritics work within notes
