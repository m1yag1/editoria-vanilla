module.exports = {
  'pubsweet-server': {
    secret:
      '42e14e00aa4b7f43e441e92476bc8633d11c0546fca1a5b5c4f83f46c79a2f30a9f21885fce7926823285b418411078f9927de471599c1f5f15f387575821046',
    db: {
      port: 5460,
      database: 'editoria_dev',
      user: 'dev',
      password: 'secretpassword',
    },
  },
  'pubsweet-client': {
    converter: 'ucp',
  },
  'pubsweet-component-ink-backend': {
    inkEndpoint: 'http://inkdemo-api.coko.foundation/',
    email: 'editoria@coko.foundation',
    password: 'editoria',
    recipes: {
      'editoria-typescript': '2',
    },
  },
  '@pubsweet/component-polling-server': {
    timer: 10000,
  },
  mailer: {
    transport: {
      auth: {
        user: 'xpub@sandbox9af3d5dc64334deca9759efe7056d6b3.mailgun.org',
        pass: 'xpubpass',
      },
    },
  },
}
