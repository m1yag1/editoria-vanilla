const path = require('path')

const components = require('./components')
const winston = require('winston')
// const authsomeMode = require('./modules/authsome')

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      colorize: true,
    }),
  ],
})

module.exports = {
  authsome: {
    mode: require.resolve('./modules/authsome'),
    teams: {
      productionEditor: {
        name: 'Production Editor',
        role: 'productionEditor',
        color: {
          addition: '#0c457d',
          deletion: '#0c457d',
        },
        weight: 1,
      },
      copyEditor: {
        name: 'Copy Editor',
        role: 'copyEditor',
        color: {
          addition: '#0ea7b5',
          deletion: '#0ea7b5',
        },
        weight: 2,
      },
      author: {
        name: 'Author',
        role: 'author',
        color: {
          addition: '#e8702a',
          deletion: '#e8702a',
        },
        weight: 3,
      },
    },
  },
  epub: {
    fontsPath: '/uploads/fonts',
  },
  'password-reset': {
    url: 'http://localhost:3000/password-reset',
    sender: 'dev@example.com',
  },
  mailer: {
    from: 'info@editoria.com',
    path: path.join(__dirname, 'mailer'),
  },
  publicKeys: [
    'authsome',
    'bookBuilder',
    'pubsweet',
    'pubsweet-client',
    'pubsweet-server',
    'validations',
    'wax',
  ],
  pubsweet: {
    components,
  },
  'pubsweet-client': {
    API_ENDPOINT: '/api',
    'login-redirect': '/',
    navigation: 'app/components/Navigation/Navigation.jsx',
    routes: 'app/routes.jsx',
    theme: 'ThemeEditoria',
    converter: 'ucp',
  },
  'pubsweet-server': {
    db: {},
    enableExperimentalGraphql: true,
    graphiql: true,
    tokenExpiresIn: '360 days',
    sse: true,
    logger,
    port: 3000,
    uploads: 'uploads',
    pool: { min: 0, max: 10, idleTimeoutMillis: 1000 },
  },
  '@pubsweet/component-polling-server': {
    timer: 10000,
  },
  wax: {
    layout: 'editoria',
    lockWhenEditing: true,
    theme: 'editoria',
    autoSave: false,
    frontmatter: {
      default: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
      component: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
    },
    body: {
      default: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
      part: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
      chapter: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
      unnumbered: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
      component: {
        menus: {},
      },
    },
    backmatter: {
      default: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
      component: {
        menus: {
          topToolBar: 'topDefault',
          sideToolBar: 'sideDefault',
          overlay: 'defaultOverlay',
        },
      },
    },
  },
  schema: {},
  validations: path.join(__dirname, 'modules', 'validations'),
}
